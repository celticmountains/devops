FROM python:3.7

# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install -r requirements.txt

# copy the content of the local src directory to the working directory
COPY src/ .

ENV FLASK_APP app.py
ENV APP_AES_KEY=somekey234876283

CMD ["flask", "run"]
