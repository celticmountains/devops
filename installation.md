# Tiqets demo instructions

## Installation

For creating the docker image and upload to your preferred docker registry you need to run the following commands
The registry for this example can be created in hub.docker.com however for production purposes it is recommended to use a private solution with security scanning.
- docker build -t daanlevi/tiqets:0.1.0 .
- docker push daanlevi/tiqets:0.1.0

## Deployment

The next step is to have a local kubernetes cluster running to install the app into. Methods for deployments can be kubernetes yaml file, Kustomize and in this case helm. It is assumed helm is installed on macOS use for example: brew install helm
- kubectl create namespace tiqets
- helm install tiqets tiqets/ -n tiqets

## Operation

Once the application is deployed you can port forward to the pod running on your kubernetes cluster.
- kubectl -n tiqets port-forward tiqets-5dd6cc5bff-nxwms  5000:5000
- echo 'hello' | curl -X POST http://localhost:5000/encrypt

## ci/cd

Here is a small GitLab file as an example for a ci/cd pipeline that can build and deploy the application automatically (still a work in progress)

## Future improvements

This is just a small selection of potential improvements.
- Multistage docker builds
- Using helm secrets for ENV APP_AES_KEY.
- Have startup, readiness and liveness probes.
- Add static code analysis to the ci/cd job.
